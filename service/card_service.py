from . import logger, app_id
from time import sleep
from client import ebay
from typing import Dict
from app.models import CategoryModel, CardModel


class CardService:
    def __init__(self):
        self.app_id = None
        self.client = None

        self.init_client()

    def init_client(self):
        """
        Initializes the EBay API client
        """

        # get app id somehow
        self.app_id = app_id
        self.client = ebay.EbayClient(app_id=self.app_id)

    def find_listings(self):
        """
        Finds the cards that are being watched
        """

        watched_items = self.query_db_for_watched_card()
        list_of_items = list()

        for item in watched_items:
            sleep(0.01)
            logger.info("Retrieving listings for card {} in category {}".format(item.name, item.category_id))

            ebay_listing = self.client.find_first_item(item.category_id, item.name)

            list_of_items.append(ebay_listing)

        return list_of_items

    def find_listings_for_card(self, card_key):
        """
        Gets listings for a specific card.
        """
        card = self.query_db_for_card(card_key)

        return self.client.find_all_items(card.category_id, card.name)

    # noinspection PyMethodMayBeStatic
    def query_db_for_watched_card(self) -> Dict:
        """
        Returns a dictionary of cards being watched to ebay category id
        """

        cards = CardModel.query.join(CategoryModel)\
            .with_entities(
                CardModel.name,
                CardModel.category_id,
                CategoryModel.ebay_category_name,
                )\
            .all()

        return cards

    # noinspection PyMethodMayBeStatic
    def query_db_for_card(self, card_key):
        """
        Query database for a single card
        """

        card = CardModel.query.\
            filter_by(name=card_key).\
            join(CategoryModel).\
            with_entities(CardModel.name, CardModel.category_id).first()
        return card
