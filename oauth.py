from os import environ


class Auth:
    def __init__(self):
        self.client_id = environ.get("GOOGLE_CLIENT_ID", None)
        self.client_secret = environ.get("GOOGLE_CLIENT_SECRET", None)
        self.auth_uri = 'https://accounts.google.com/o/oauth2/auth'
        self.token_uri = 'https://accounts.google.com/o/oauth2/token'
        self.user_info = 'https://www.googleapis.com/userinfo/v2/me'
        self.scope = ['profile', 'email']
        self.redirect_uri = environ.get("REDIRECT_URI", 'http://localhost:9090/callback')

    def __repr__(self):
        return repr([self.client_id, self.redirect_uri])
